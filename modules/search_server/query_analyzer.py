#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re
import time

import json
import requests
from flask import Flask, jsonify, request
from flask_cors import CORS
from pymongo import MongoClient
from pymongo.errors import AutoReconnect


import settings
from modules.PostInfo import PostInfo
from modules.make_up.make_up import make_up
import modules.make_up.miscellaneous.normalize.convention as convention


app = Flask(__name__)
CORS(app)
model_url = "http://35.240.240.251/api/v1/real-estate-extraction"


client = MongoClient(settings.MONGO_LINK)
coll = client[settings.MONGO_DB][settings.MONGO_DATA]
WEIGHTS = {
    "potential": 12,
    "surrounding": 10,
    "surrounding_characteristics": 8,
    "surrounding_name": 20
}

inf_score = 3

# these fields listed in the following will be checked and sent to MongoDB
useful_attributes = [
    "attr_addr_number",
    "attr_addr_street",
    "attr_addr_district",
    "attr_addr_ward",
    "attr_addr_city",
    "attr_surrounding",
    "attr_surrounding_name",
    "attr_transaction_type",
    "attr_realestate_type",
    "attr_potential",
    "attr_area",
    "attr_price",
    "attr_position"
]

DEFAULT_LIMIT = 25

@app.route('/post_suggestor', methods=['POST'])
def analyze_query():
    ''' Analyze query from user
    Include: normalize data, calculate score, send request to MongoDB and process response
    '''
    req = request.json
    limit = 30
    
    if isinstance(req, str):
        query = req


        res = requests.post(model_url, json=[query]).json()[0]

    else:
        if req.get('string', True):
            query = req['query']
            try:
                limit = req['limit']
                if limit > DEFAULT_LIMIT:
                    limit = DEFAULT_LIMIT
            except KeyError:
                limit = DEFAULT_LIMIT

            res = requests.post(model_url, json=[query]).json()[0]


        skip = req.get('skip', 0)

    # ****************************************************************************************
    # ************************ BASIC PROCESSING REQUEST **************************************
    # ****************************************************************************************

    # req = request.json

    user_request = PostInfo()
    user_request.set_info('message', req['query'])
    make_up(user_request)

    # ****************************************************************************************
    # ************************** DEEPER PROCEESING TAGS OF REQUEST ***************************
    # ****************************************************************************************



    final_tags = {}
    for attribute in useful_attributes:
        tmp = user_request.get_info(attribute)
        if tmp is not None and tmp != "":
            final_tags[attribute] = tmp

    # *** BACKUP query before sending ***
    with open('final_tag.json', 'w') as out:
        json.dump(final_tags, out, indent=4)

    

    # ****************************** make up request to send to MongoDB ********************
    secondary = []
    score = []
    match = []
    for k, v in final_tags.items():


        # if k.startswith('surrounding') or k == 'potential':
        #     v = [remove_accents(re.sub(r"\W+|", "", x)) for x in v]

        #     secondary.append({
        #         "norm_val.{}".format(k): {
        #             "$in": ["{0}|{1}".format(remove_accents(x),k) for x in v]
        #         }
        #     })
        #     score.extend({
        #         "$cond": [
        #             {
        #                 "$and": [
        #                     {
        #                         "$gt": ["$norm_val.{}".format(k), None]
        #                     },
        #                     {
        #                         "$not":[
        #                             {
        #                                 "$in": ["{0}|{1}".format(remove_accents(x),k), "$norm_val.{}".format(k)]
        #                             }
        #                         ]
        #                     }
        #                 ]
        #             },
        #             WEIGHTS[k],
        #             0
        #         ]
        #     } for x in v)
        #     # text_search.update(remove_accents(x) for x in v)

        # elif k.startswith('interior'):
        #     for k1, v1 in v.items():
        #         match.append({
        #             "norm_val.{}.{}".format(k, k1): {
        #                 "$elemMatch": {
        #                     "$or": [
        #                         {
        #                             "low": {"$lte": x['high']},
        #                             "high":{"$gte": x['low']}
        #                         } for x in v1
        #                     ]
        #                 }
        #             }
        #         })





        if k == 'attr_price':
            price_min = final_tags["attr_price"][0]
            price_max = final_tags["attr_price"][1]
            
            if price_min * price_max ==  0:
                continue

            if price_min == -1:   # "duoi" is available
                match.append(
                    {
                        'attr_price_min': 
                            {
                                '$lte': price_max * 1.05
                            }
                    }
                )
            elif price_max == -1:   # "tren" is available
                match.append(
                    {
                        'attr_price_min': 
                            {
                                '$gte': price_min * 0.95
                            }
                    }
                )
            else:
                match.append(
                    {
                        'attr_price_min': 
                            {
                                '$gte': price_min * 0.95, 
                                '$lte': price_max * 1.05
                            }
                    }
                )


        elif k == 'attr_area':
            # there are 4 possible scenarios:
            #   tren 50m
            #   duoi 60m
            #   tu 30 den 60m
            #   tam 40m
            # 2 lower cases are resolved in making up data step

            area_min = final_tags["attr_area"][0]
            area_max = final_tags["attr_area"][1]

            if area_min is None:
                continue


            if area_min == -1:   # "duoi" is available
                match.append(
                    {
                        'attr_area': 
                            {
                                '$lte': area_max * 1.05
                            }
                    }
                )
            elif area_max == -1:   # "tren" is available
                match.append(
                    {
                        'attr_area': 
                            {
                                '$gte': area_min * 0.95
                            }
                    }
                )
            else:
                match.append(
                    {
                        'attr_area': 
                            {
                                '$gte': area_min * 0.95, 
                                '$lte': area_max * 1.05
                            }
                    }
                )


    
        elif k == 'attr_addr_street': 
            match.append(
                {
                    'attr_addr_street': {"$regex" : final_tags['attr_addr_street']}
                }
            )
            

        
        elif k == "attr_addr_district":
            match.append(
                {
                    'attr_addr_district': {"$regex" : final_tags['attr_addr_district']}
                }
            )

        # elif k in ['attr_transaction_type', 'attr_realestate_type', 'attr_addr_city']:
        #     match.append(
        #         {
        #             k: item
        #         }
        #     )

        else:
            match.append(
                {
                    k: v
                }
            )


    query = []
    if len(match) > 0:
        query.append({
            "$match": {
                "$and": match
            }
        })
    if len(secondary) > 0:
        query.append({
            "$match": {
                "$or": secondary
            }
        })
    # rt = final_tags.get('realestate_type')
    # project = {
    #     "_id": 1,
    #     "content": 1,
    #     "title": 1,
    #     "date": 1
    # }


    # query.append({"$project": project})
    # query.append({"$sort": {"score": 1, "date": -1}})
    # query.append({'$skip': skip})
    query.append({"$limit": limit})
    
    with open('query.json', 'w') as out:
        json.dump(query, out, indent=4)



    # ************************************************************************************
    # ***************************** SEND REQUEST AND PROCESS RESPONSE ********************
    # ************************************************************************************

    for _ in range(60):
        try:
            res = list(coll.aggregate(query))
            break
        except AutoReconnect as e:
            time.sleep(5)


    response = []
    for post in res:
        response.append({
            "post_id"   : post['post_id'],
            "content"   : post['message'],
            "post_date" : post['post_date'],
            "title"     : post['title'],
            "score"     : 0,
            "link"      : post['link']
        })
  
    # print(response)


    return jsonify({
        'data': response
    })

    # print(res)
    # return jsonify(

    # )

    # data = {'data': res[0]}
    # response = app.response_class(response=json.dumps(data),
    #                               status=200,
    #                               mimetype='application/json')
    # return response
    # data = {'data': res}
    # return jsonify(data)


    # def zscore_price(price):
    #     return (price - 13905.538968548684)/167162.46843965404

    # def zscore_area(area):
    #     return (area - 8917.27633785681)/2087892.246881467

    # def norm_surrouding(score):
    #     return (score/50)*0.00001;

    # def euclidean_dist(x):
    #     try:
    #         request_price = final_tags['price'][0]['low']
    #         price_diff_list = [abs(request_price - k['low']) for k in x['norm_val']['price']]
    #         price1=x['norm_val']['price'][price_diff_list.index(min(price_diff_list))]['low']
    #     except:
    #         request_price = 0
    #         price1 = 0

    #     try:
    #         request_area = final_tags['area'][0]['low']['dien tich']
    #         area_diff_list = [abs(request_area - k['low']['dien tich']) for k  in x['norm_val']['area']]
    #         area1 = x['norm_val']['area'][area_diff_list.index(min(area_diff_list))]['low']['dien tich']
    #     except:
    #         request_area = 0
    #         area1 = 0

    #     vector_item1 = (zscore_price(price1) if price1!=0 else 0, zscore_area(area1) if area1!=0 else 0, norm_surrouding(x['score']))
    #     request_item = (zscore_price(request_price) if request_price!= 0 else 0, zscore_area(request_area) if request_area!=0 else 0, 0)
    #     return distance.euclidean(vector_item1, request_item)

    # def cmp_zscore(x, y):
    #     return x['edist'] - y['edist']


    # # calculate Euclidean distance and sort the posts by their Euclidean distance
    # for item in res:
    #     item['edist'] = euclidean_dist(item)

    # res.sort(key=cmp_to_key(cmp_zscore))

    # print(res)


def execute_server():
    app.run(host='0.0.0.0', debug=True, port=47263)
