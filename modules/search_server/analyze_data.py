from pymongo import MongoReplicaSetClient, MongoClient
from pymongo.errors import AutoReconnect
import numpy as np
import matplotlib.pyplot as plt
import math

database_url = "localhost:27017"
db = MongoClient(database_url)["real-estate"]
coll = db["data_post_norm"]

contents = []
data = []
def read():
    empcol = coll.find().hint([('$natural', 1)])
    for emp in empcol:
        contents.append(emp["norm_val"])
        data.append(emp)

read()
price = []
i = 0
area = []
# for item in contents:
#     item_price_list = item['price'] if item.get('price',None)!= None and len(item.get('price',None))>0 else 0
#     newlist = []
#     if item_price_list != 0:
#         for ind in item_price_list:
#             newlist.append(ind['low'])
#         item_price = min(newlist)
#     else:
#         item_price = 0
#     if item_price == 45000000:
#         print(data[i]['content'])
#         print("Price: ", item_price)
#     if item_price!= 0 and item_price < 1e10:
#         price.append(item_price)
#     i+=1
#
for item in contents:
    item_area_list = item['area'] if item.get('area',None) != None and len(item.get('area',None))>0 and item['area'][0]['low'].get('dien tich',None)!= None else 0
    newlist =[]
    if isinstance(item_area_list, list) :
        for ind in item_area_list:
            if isinstance(ind['low'], dict) and ind['low'].get('dien tich', None)!= None:
                newlist.append(ind['low']['dien tich'])
        item_area = min(newlist)
    else:
        item_area = 0
    # if item_area == 637000000:
    #     print(data[i]['content'])
    #     print("Area:  ", item_area)
    if item_area > 0:
        area.append(item_area)
    i+=1

# plt.hist(area, bins=np.arange(min(area), 1000))
# plt.show()

# print("Mean: ", np.mean(price))
# print("Standard deviation: ", np.std(price))
# print("Min: ", min(price))
# print("Max; ", max(price))


print("Mean: ", np.mean(area))
print("Standard deviation: ", np.std(area))
print("Min: ", min(area))
print("Max; ", max(area))