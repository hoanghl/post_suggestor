from __future__ import print_function
import csv

def alias_city(filename):
    with open(filename, newline='', encoding='utf-8') as csvfile:
        return list(csv.reader(csvfile, delimiter=','))
