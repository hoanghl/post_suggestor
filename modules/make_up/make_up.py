from re import findall
from re import search

from modules.make_up.api.api_NLP_communicate import get_from_api
from modules.make_up.miscellaneous.normalize.utils import *
from modules.make_up.miscellaneous.regex_operation import *


filling_attrs = [
    "attr_addr_number",
    "attr_addr_street",
    "attr_addr_district",
    "attr_addr_ward",
    "attr_addr_city",
    "attr_position",
    "attr_surrounding",
    "attr_surrounding_name",
    "attr_surrounding_characteristics",
    "attr_transaction_type",
    "attr_realestate_type",
    "attr_potential",
    "attr_area",
    "attr_price",
    "attr_interior_floor",
    "attr_interior_room",
    "attr_orientation",
    "attr_project",
    "attr_legal",
]


def make_up(post_info):
    """
        try to fill in the keys in PostInfo
    :return:
    """
    print("====================== MAKE UP DATA =========================")

    # get attributes of post from NLP API and Google Geo API
    attributes = get_attributes(post_info)
    price_min, price_max = get_price(post_info, attributes)

    area_min, area_max   = get_area(post_info, attributes)

    for attribute in filling_attrs:
        if post_info.get_info(attribute) is None:
            if attribute == "attr_price":
                post_info.set_info(attribute, [price_min, price_max])
            elif attribute == "attr_area":
                post_info.set_info(attribute, [area_min, area_max])             

            else:
                post_info.set_info(attribute, attributes[attribute])

   


    ## standardize some geographical location name
    post_info.set_info("attr_addr_city",        normalize_city(post_info.get_info("attr_addr_city")))
    post_info.set_info("attr_transaction_type", normalize_transaction_type(post_info.get_info("attr_transaction_type")))
    post_info.set_info("attr_realestate_type",  normalize_realestate_type(post_info.get_info("attr_realestate_type")))
    post_info.set_info("attr_addr_district",    normalize_district(post_info.get_info("attr_addr_district")))
    post_info.set_info("attr_position",         normalize_position(post_info.get_info("attr_position")))

def get_attributes(post_info):
    tmp = get_from_api(post_info.get_info('message'))

    if post_info.get_info('address'):
        tmp1 = get_from_api(post_info.get_info('address'))
        tmp['attr_addr_number']   = tmp1['attr_addr_number']
        tmp['attr_addr_street']   = tmp1['attr_addr_street']
        tmp['attr_addr_district'] = tmp1['attr_addr_district']
        tmp['attr_addr_ward']     = tmp1['attr_addr_ward']

    if post_info.get_info('city'):
        tmp['attr_addr_city'] = post_info.get_info('city')

    if post_info.get_info('attr_transaction_type'):
        tmp['attr_transaction_type'] = post_info.get_info('attr_transaction_type')

    if post_info.get_info('attr_realestate_type'):
        tmp['attr_realestate_type'] = post_info.get_info('attr_realestate_type')

    if post_info.get_info('attr_area'):
        tmp['attr_area'] = post_info.get_info('attr_area')

    if post_info.get_info('attr_legal'):
        tmp['attr_legal'] = post_info.get_info('attr_legal')

    if post_info.get_info('attr_orientation'):
        tmp['attr_orientation'] = post_info.get_info('attr_orientation')

    if post_info.get_info('bedroom'):
        tmp['attr_interior_room'] = int(post_info.get_info('bedroom'))

    if post_info.get_info('bathroom'):
        tmp['attr_interior_room'] += int(post_info.get_info('bathroom'))

    return tmp

    
def get_price(post_info, attributes):
    if post_info.get_info('attr_price') is not None:
        return normalize_price(post_info.get_info('attr_price'))
    else:
        price_min = 0
        price_max = 0

        for tmp in attributes['attr_price']:
            price = normalize_price(tmp)
            if price_min == 0 and price[0]:
                price_min = price[0]
            if price_max == 0 and price[1]:
                price_max = price[1]

        # if reach here that means not any one extracted by NLP API is valueable
        return price_min, price_max

# regex for area
re_area = r"(\d+\.\d+|\d+)"

def get_area(post_info, attributes):
    if len(attributes['attr_area']) == 0:
        return None, None
    str_price = attributes['attr_area'][0].replace(" ", "").replace("m2", "").replace("m", "")

    # print("aaa: ", str_price)
    # exit()

    str_price_parts = re.findall(re_area, str_price)

    # if "tren" is available:
    #  area_min = float and area_max = -1
    # if "duoi" is available:
    #  area_min = -1 and area_max = float
    # else nothing happens
    if search(r"tren|hon", str_price):
        area_min = float(str_price_parts[0])
        area_max = -1
    elif search(r"duoi", str_price):
        area_min = -1
        area_max = float(str_price_parts[0])
    else:
        area_min = float(str_price_parts[0])
        area_max = float(str_price_parts[1]) if len(str_price_parts) == 2 else area_min

    return area_min, area_max


    
    
