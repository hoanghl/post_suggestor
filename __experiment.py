from pymongo import MongoClient
from time import sleep
import settings

client = MongoClient(settings.MONGO_LINK)
# client = MongoClient('localhost:27017')
collection = client[settings.MONGO_DB][settings.MONGO_DATA]
# data = collection.aggregate(
#     [
#         {"$match": 
#             {
#                 "$and": [
#                     {
#                         "norm_val.attr_transaction_type": { "$in": [ "bán", "$attr_transaction_type"       ] }
#                     },
#                     # {
#                     #     "norm_val.attr_realestate_type" : { "$in": [ "nh\u00e0|attr_realestate_type"        ] }
#                     # },
#                     # {
#                     #     "norm_val.attr_addr_city"       : { "$in": [ "H\u1ed3_Ch\u00ed_Minh|attr_addr_city" ] }
#                     # }
#                 ]
#             }
#         }
#         # # {"$limit": 20},
#         # {"$group" :
#         #     {"_id" : 
#         #         {
#         #             "post_id" : "$post_id", 
#         #             "longtitude": "$location_lng",
#         #             "latitude": "$location_lat",
#         #             "area": "$attr_area",
#         #             "price_min": "$attr_price_min",
#         #             "price": "$attr_price"

#         #         }
#         #     },
#         # }
#     ]
# )

# data = collection.aggregate(
#     [
#         {"$match": 
#             {
#                 # "$in" : ["bán", "$attr_transaction_type"]
#                 "$and" : [
                    
#                     {"attr_transaction_type" : "thu\u00ea"},
#                     {"attr_realestate_type"  : "nh\u00e0"},
#                     {
#                         "$or" : [
#                             {"attr_addr_city"        : "H\u1ed3 Ch\u00ed Minh"},
#                             {
#                             "attr_addr_city": "hcm"
#                             },
#                             {
#                             "attr_addr_city": "s\u00e0i g\u00f2n"
#                             },
#                             {
#                             "attr_addr_city": "ho chi minh"
#                             },
#                             {
#                             "attr_addr_city": "tp . hcm"
#                             }
#                         ]
#                     }
#                 ]
#             }
#         },
#         {"$limit": 2},
#         # {"$group" :
#         #     {"_id" : 
#         #         {
#         #             "post_id" : "$post_id", 
#         #             "longtitude": "$location_lng",
#         #             "latitude": "$location_lat",
#         #             "area": "$attr_area",
#         #             "price_min": "$attr_price_min",
#         #             "price": "$attr_price"

#         #         }
#         #     },
#         # }
#     ]
# )

data = collection.aggregate(
    [
    {
        "$match": {
            "$and": [
                {
                    "attr_addr_city": "\u0111\u1ed3ng nai"
                },
                {
                    "attr_transaction_type": "b\u00e1n"
                },
                {
                    "attr_realestate_type": "nh\u00e0"
                },
                {
                    "attr_price_min": {
                        "$lte": 1900000000.0
                    }
                }
            ]
        }
    },
]
)

print(list(data))

# data = collection.aggregate(
#     [
#     {
#         "$match": {
#             "$and": [
#                 {
#                     "attr_addr_district": {
#                         "$regex": "t\u00e2n b\u00ecnh"
#                     }
#                 },
#                 {
#                     "$or": [
#                         {
#                             "attr_transaction_type": "b\u00e1n"
#                         },
#                         {
#                             "attr_transaction_type": "B\u00e1n"
#                         },
#                         {
#                             "attr_transaction_type": "b\u1ea5n"
#                         },
#                         {
#                             "attr_transaction_type": "ban"
#                         }
#                     ]
#                 },
#                 {
#                     "$or": [
#                         {
#                             "attr_realestate_type": "d\u00e2t"
#                         },
#                         {
#                             "attr_realestate_type": "d\u00e3y tr\u1ecd"
#                         },
#                         {
#                             "attr_realestate_type": "d\u1ea1y tr\u1ecd"
#                         },
#                         {
#                             "attr_realestate_type": "k\u0111t"
#                         },
#                         {
#                             "attr_realestate_type": "n\u1ec1n"
#                         },
#                         {
#                             "attr_realestate_type": "n\u1ec1n bi\u1ec7t th\u1ef1"
#                         },
#                         {
#                             "attr_realestate_type": "v\u01b0\u1eddn b\u01b0\u1edfi"
#                         },
#                         {
#                             "attr_realestate_type": "v\u01b0\u1eddn b\u1eb1ng"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u00e1t"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u00e2t"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u00e2t n\u1ec1n"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u1ea4T"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u1ea5t"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u1ea5t n\u00f4ng nghi\u1ec7p"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u1ea5t n\u1ec1n"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u1ea5t n\u1ec1n bi\u1ec7t th\u1ef1"
#                         },
#                         {
#                             "attr_realestate_type": "\u0111\u1ea5t thu\u1ed1c"
#                         }
#                     ]
#                 },
#                 # {
#                 #     "attr_area": {
#                 #         "$gte": 47.5
#                 #     }
#                 # }
#             ]
#         }
#     },
# ]
# )
# import re
# re_area = r"(\d+\.\d+|\d+)"
# for id in collection.distinct("post_id", {"page" : "https://mogi.vn/"}):
#     area = re.findall(re_area, collection.distinct("attr_area", {"post_id" : id})[0])
#     print(area)
#     collection.update_one({"post_id" : id}, {"$set" : {"attr_area" : area}})

# client.close()
# # from modules.make_up.miscellaneous.normalize.utils import debug
# # debug()

# import requests
# content = "Cần mua nhà ở quận 1"
# dictToSend = {'query': content }
# data_list = [content]

# request = requests.Session()
# res = requests.post("http://127.0.0.1:47263/post_suggestor",json=dictToSend)
# data = res.json()['data']
# for dat in data:
#     print(dat)
#print(res['data'])